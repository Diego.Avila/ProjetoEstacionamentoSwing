/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author Diego.Avila
 */
public class ClientesDetalhes {
    private Long id;
    private String placa;
    private long idcliente;
    private long idendereco;
    private long idveiculo;

    public ClientesDetalhes(Long id, String placa, long idcliente, long idendereco, long idveiculo) {
        this.id = id;
        this.placa = placa;
        this.idcliente = idcliente;
        this.idendereco = idendereco;
        this.idveiculo = idveiculo;
    }

    public ClientesDetalhes() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(long idcliente) {
        this.idcliente = idcliente;
    }

    public long getIdendereco() {
        return idendereco;
    }

    public void setIdendereco(long idendereco) {
        this.idendereco = idendereco;
    }

    public long getIdveiculo() {
        return idveiculo;
    }

    public void setIdveiculo(long idveiculo) {
        this.idveiculo = idveiculo;
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientesDetalhes other = (ClientesDetalhes) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClientesDetalhes{" + "id=" + id + '}';
    }
    
    
   
}

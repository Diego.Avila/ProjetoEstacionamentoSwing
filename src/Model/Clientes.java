package Model;

import java.sql.Date;

/**
 *
 * @author Diego.Avila
 */
public class Clientes extends Pessoa {

    private String cpf;
    private Date datanascimento;
    private String estadocivil;



    

    public Clientes() {
    }

    public Clientes(String cpf, Date datanascimento, String estadocivil, long id, String nome, String cidade, String telefone1, String telefone2) {
        super(id, nome, cidade, telefone1, telefone2);
        this.cpf = cpf;
        this.datanascimento = datanascimento;
        this.estadocivil = estadocivil;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDatanascimento() {
        return datanascimento;
    }

    public void setDatanascimento(Date datanascimento) {
        this.datanascimento = datanascimento;
    }

    public String getEstadocivil() {
        return estadocivil;
    }

    public void setEstadocivil(String estadocivil) {
        this.estadocivil = estadocivil;
    }

    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Time;

/**
 *
 * @author Diego.Avila
 */
public class HoraVeiculo {
    private int id;
    private Time tempo;
    private double valor;
    private int idTipo;

    public HoraVeiculo(int id, Time tempo, double valor, int idTipo) {
        this.id = id;
        this.tempo = tempo;
        this.valor = valor;
        this.idTipo = idTipo;
    }

    public HoraVeiculo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Time getTempo() {
        return tempo;
    }

    public void setTempo(Time tempo) {
        this.tempo = tempo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    
    
}

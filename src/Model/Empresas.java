/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Diego.Avila
 */
public class Empresas extends Pessoa{
    private String cnpj;
    private String estado;

    public Empresas(String cnpj, String estado, long id, String nome, String cidade, String telefone1, String telefone2) {
        super(id, nome, cidade, telefone1, telefone2);
        this.cnpj = cnpj;
        this.estado = estado;
    }

    public Empresas() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    

    
}
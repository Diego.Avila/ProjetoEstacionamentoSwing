/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;



/**
 *
 * @author Maiara-PC
 */
public class Mensalistas {
   private long id;
   private long idpessoasfisicas;
   private long idpessoasjuridicas;
   private long idveiculos;
   private Date iniciocontrato;
   private Date finalcontrato;

    public Mensalistas(long id, long idpessoasfisicas, long idpessoasjuridicas, long idveiculos, Date iniciocontrato, Date finalcontrato) {
        this.id = id;
        this.idpessoasfisicas = idpessoasfisicas;
        this.idpessoasjuridicas = idpessoasjuridicas;
        this.idveiculos = idveiculos;
        this.iniciocontrato = iniciocontrato;
        this.finalcontrato = finalcontrato;
    }

    public Mensalistas() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdpessoasfisicas() {
        return idpessoasfisicas;
    }

    public void setIdpessoasfisicas(long idpessoasfisicas) {
        this.idpessoasfisicas = idpessoasfisicas;
    }

    public long getIdpessoasjuridicas() {
        return idpessoasjuridicas;
    }

    public void setIdpessoasjuridicas(long idpessoasjuridicas) {
        this.idpessoasjuridicas = idpessoasjuridicas;
    }

    public long getIdveiculos() {
        return idveiculos;
    }

    public void setIdveiculos(long idveiculos) {
        this.idveiculos = idveiculos;
    }

    public Date getIniciocontrato() {
        return iniciocontrato;
    }

    public void setIniciocontrato(Date iniciocontrato) {
        this.iniciocontrato = iniciocontrato;
    }

    public Date getFinalcontrato() {
        return finalcontrato;
    }

    public void setFinalcontrato(Date finalcontrato) {
        this.finalcontrato = finalcontrato;
    }
  

   
   
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mensalistas other = (Mensalistas) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mensalista{" + "id=" + id + '}';
    }
   
   
   
}

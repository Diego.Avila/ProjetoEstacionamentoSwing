/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author Diego.Avila
 */
public class Veiculos {
    private long id;
    private long idmarcasveiculos;
    private long idcores;

    public Veiculos(long id, long idmarcasveiculos, long idcores) {
        this.id = id;
        this.idmarcasveiculos = idmarcasveiculos;
        
        this.idcores = idcores;
    }

    public Veiculos() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdmarcasveiculos() {
        return idmarcasveiculos;
    }

    public void setIdmarcasveiculos(long idmarcasveiculos) {
        this.idmarcasveiculos = idmarcasveiculos;
    }


    public long getIdcores() {
        return idcores;
    }

    public void setIdcores(long idcores) {
        this.idcores = idcores;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Veiculos other = (Veiculos) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Veiculos{" + "id=" + id + '}';
    }
    

}

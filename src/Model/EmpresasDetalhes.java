/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Diego.Avila
 */
public class EmpresasDetalhes {
    private long id;
    private String placa;
    private long idempresas;
    private long idveiiculos;
    private long idenderecos;

    public EmpresasDetalhes(long id, String placa, long idempresas, long idveiiculos, long idenderecos) {
        this.id = id;
        this.placa = placa;
        this.idempresas = idempresas;
        this.idveiiculos = idveiiculos;
        this.idenderecos = idenderecos;
    }

    public EmpresasDetalhes() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public long getIdempresas() {
        return idempresas;
    }

    public void setIdempresas(long idempresas) {
        this.idempresas = idempresas;
    }

    public long getIdveiiculos() {
        return idveiiculos;
    }

    public void setIdveiiculos(long idveiiculos) {
        this.idveiiculos = idveiiculos;
    }

    public long getIdenderecos() {
        return idenderecos;
    }

    public void setIdenderecos(long idenderecos) {
        this.idenderecos = idenderecos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpresasDetalhes other = (EmpresasDetalhes) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EmpresasDetalhes{" + "id=" + id + '}';
    }

    
}

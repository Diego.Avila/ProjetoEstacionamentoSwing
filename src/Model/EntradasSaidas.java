/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;



/**
 *
 * @author Diego.Avila
 */
public class EntradasSaidas {
//ENTRADA
private long id;
private Date dataentrada;
private String placa;
private String observacao;
private long idveiculo;
private long idusuario=1;//por enquanto esta iniciado em 1
//SAIDA
private Date datasaida;
private double valor;

    public EntradasSaidas(long id, Date dataentrada, String placa, String observacao, long idveiculo, Date datasaida, double valor) {
        this.id = id;
        this.dataentrada = dataentrada;
        this.placa = placa;
        this.observacao = observacao;
        this.idveiculo = idveiculo;
        this.datasaida = datasaida;
        this.valor = valor;
    }

    public EntradasSaidas() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDataentrada() {
        return dataentrada;
    }

    public void setDataentrada(Date dataentrada) {
        this.dataentrada = dataentrada;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public long getIdveiculo() {
        return idveiculo;
    }

    public void setIdveiculo(long idveiculo) {
        this.idveiculo = idveiculo;
    }

    public long getIdusuario() {
        return idusuario;
    }
//Não deve ser setado
//    public void setIdusuario(long idusuario) {
//        this.idusuario = idusuario;
//    }

    public Date getDatasaida() {
        return datasaida;
    }

    public void setDatasaida(Date datasaida) {
        this.datasaida = datasaida;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntradasSaidas other = (EntradasSaidas) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntradasSaidas{" + "id=" + id + '}';
    }



}

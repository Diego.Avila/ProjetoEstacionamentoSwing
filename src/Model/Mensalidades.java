/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author Diego.Avila
 */
public class Mensalidades {
    private long id;
    private double valor;
    private Date datapagamento;
    private String observacao;
    private long idmensalistas;

    public Mensalidades(long id, double valor, Date datapagamento, String observacao, long idmensalistas) {
        this.id = id;
        this.valor = valor;
        this.datapagamento = datapagamento;
        this.observacao = observacao;
        this.idmensalistas = idmensalistas;
    }

    public Mensalidades() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getDatapagamento() {
        return datapagamento;
    }

    public void setDatapagamento(Date datapagamento) {
        this.datapagamento = datapagamento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public long getIdmensalistas() {
        return idmensalistas;
    }

    public void setIdmensalistas(long idmensalistas) {
        this.idmensalistas = idmensalistas;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mensalidades other = (Mensalidades) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mensalidades{" + "id=" + id + '}';
    }
    
    
    
    
    
}

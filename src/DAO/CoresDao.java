/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static DAO.ConexaoDao.getConexao;
import Model.Cores;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Diego.Avila
 */
public class CoresDao extends ConexaoDao {

    public long inserir(Cores objeto) throws SQLException {
        setSql(" INSERT INTO cores (cor) "
                + "VALUES (?)");

        try {
            setPs(getConexao().prepareStatement(getSql(),getSt().RETURN_GENERATED_KEYS));
            getPs().setString(1, objeto.getCor());
            getPs().executeUpdate();
            setResultSet(getPs().getGeneratedKeys());
            if (getResultSet().next()) {
                setRetorno(getResultSet().getLong(1));
                System.out.println("Inserido ---------------id = "+ getRetorno());
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro no cadastro dao ," + ex.getMessage());

        }
        return getRetorno();
    }

    public void atualizar(Cores objeto) {
        setSql(" UPDATE cores set cor= ? WHERE id= ?  ");
        try {
            setPs(getConexao().prepareStatement(getSql()));
            getPs().setString(1, objeto.getCor());
            getPs().setLong(2, objeto.getId());
            System.out.println(getPs());
            if (getPs().executeUpdate() > 0) {

                System.out.println("Atualizado ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro-->>>  " + ex);

        }
    }

    
    public void deletar(String id) {
        setSql(" delete from cores where id =" + id);
        try {
            setPs(getConexao().prepareStatement(getSql()));
            if (getPs().executeUpdate() > 0) {
                System.out.println("Deletando ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
    }

    
    public void listarJTable(JTable tabelaCores) {

        try {

            DefaultTableModel modelo = new DefaultTableModel();
            tabelaCores.setModel(modelo);
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select * from cores  order by cor"));
            java.sql.ResultSetMetaData rsMd = getResultSet().getMetaData();
            int quantidadeColunas = rsMd.getColumnCount();
            for (int i = 1; i <= quantidadeColunas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            while (getResultSet().next()) {
                Object[] fila = new Object[quantidadeColunas];
                for (int i = 0; i < quantidadeColunas; i++) {
                    fila[i] = getResultSet().getObject(i + 1);
                }
                modelo.addRow(fila);
            }
            getResultSet().close();
            getConexao().close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Cores> listaCor() throws SQLException {
        List<Cores> lista = new ArrayList<Cores>();
        try {
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select id , cor  from cores  order by cor"));
            while (getResultSet().next()) {
                Cores cores = new Cores();
                cores.setId(getResultSet().getLong("id"));
                cores.setCor(getResultSet().getString("cor"));
                lista.add(cores);
            }
        } catch (Exception ex) {
            System.out.println("ERRO->>>" + ex);
        }
        getResultSet().close();
        getConexao().close();
        return lista;
    }

   

}
















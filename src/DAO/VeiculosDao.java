/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Veiculos;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Diego.Avila
 */
public class VeiculosDao extends ConexaoDao{
    
     public long inserir(Veiculos objeto) throws SQLException {
        setSql(" INSERT INTO veiculos (idmarcasveiculos, idcores) "
                + "VALUES (?,?,?)");

        try {
            setPs(getConexao().prepareStatement(getSql(),getSt().RETURN_GENERATED_KEYS));
            getPs().setLong(1, objeto.getIdmarcasveiculos());
            getPs().setLong(2, objeto.getIdcores());
            getPs().executeUpdate();
            setResultSet(getPs().getGeneratedKeys());
            if (getResultSet().next()) {
                setRetorno(getResultSet().getLong(1));
                System.out.println("Inserido ---------------id = "+ getRetorno());
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro no cadastro dao ," + ex.getMessage());

        }
        return getRetorno();
    }

    public void atualizar(Veiculos objeto) {
        setSql(" UPDATE veiculos set idmarcasveiculos = ?, set idcores = ? WHERE id= ?  ");
        try {
            setPs(getConexao().prepareStatement(getSql()));
            getPs().setLong(1, objeto.getIdmarcasveiculos());
            getPs().setLong(2, objeto.getIdcores());
            getPs().setLong(3, objeto.getId());
            System.out.println(getPs());
            if (getPs().executeUpdate() > 0) {

                System.out.println("Atualizado ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro-->>>  " + ex);

        }
    }

    
    public void deletar(String id) {
        setSql(" delete from veiculos where id =" + id);
        try {
            setPs(getConexao().prepareStatement(getSql()));
            if (getPs().executeUpdate() > 0) {
                System.out.println("Deletando ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
    }

    
    public void listarJTable(JTable tabelaCores) {

        try {

            DefaultTableModel modelo = new DefaultTableModel();
            tabelaCores.setModel(modelo);
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select * from veiculos  order by id"));
            java.sql.ResultSetMetaData rsMd = getResultSet().getMetaData();
            int quantidadeColunas = rsMd.getColumnCount();
            for (int i = 1; i <= quantidadeColunas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            while (getResultSet().next()) {
                Object[] fila = new Object[quantidadeColunas];
                for (int i = 0; i < quantidadeColunas; i++) {
                    fila[i] = getResultSet().getObject(i + 1);
                }
                modelo.addRow(fila);
            }
            getResultSet().close();
            getConexao().close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Veiculos> listaVeiculos() throws SQLException {
        List<Veiculos> lista = new ArrayList<Veiculos>();
        try {
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select id , idmarcasveiculos, idcores  from veiculos  order by id"));
            while (getResultSet().next()) {
                Veiculos cores = new Veiculos();
                cores.setId(getResultSet().getLong("id"));
                cores.setIdmarcasveiculos(getResultSet().getLong("idmarcasveiculos"));
                cores.setIdcores(getResultSet().getLong("idcores"));
                lista.add(cores);
            }
        } catch (Exception ex) {
            System.out.println("ERRO->>>" + ex);
        }
        getResultSet().close();
        getConexao().close();
        return lista;
    }

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.MarcasVeiculos;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Diego.Avila
 */
public class MarcasVeiculosDao extends ConexaoDao{
    
     public long inserir(MarcasVeiculos objeto) throws SQLException {
        setSql(" INSERT INTO marcasVeiculos (marca, idcategoria) "
                + "VALUES (?,?)");

        try {
            setPs(getConexao().prepareStatement(getSql(),getSt().RETURN_GENERATED_KEYS));
            getPs().setString(1, objeto.getMarca());
            getPs().setLong(2, objeto.getIdcategoria());
            getPs().executeUpdate();
            setResultSet(getPs().getGeneratedKeys());
            if (getResultSet().next()) {
                setRetorno(getResultSet().getLong(1));
                System.out.println("Inserido ---------------id = "+ getRetorno());
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro no cadastro dao ," + ex.getMessage());

        }
        return getRetorno();
    }

    public void atualizar(MarcasVeiculos objeto) {
        setSql(" UPDATE marcasVeiculos set marca= ? WHERE id= ?  ");
        try {
            setPs(getConexao().prepareStatement(getSql()));
            getPs().setString(1, objeto.getMarca());
            getPs().setLong(2, objeto.getId());
            System.out.println(getPs());
            if (getPs().executeUpdate() > 0) {

                System.out.println("Atualizado ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro-->>>  " + ex);

        }
    }

    
    public void deletar(String id) {
        setSql(" delete from marcasVeiculos where id =" + id);
        try {
            setPs(getConexao().prepareStatement(getSql()));
            if (getPs().executeUpdate() > 0) {
                System.out.println("Deletando ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
    }

    
    public void listarJTable(JTable tabelaMarcas, long idCategoria) {

        try {

            DefaultTableModel modelo = new DefaultTableModel();
            tabelaMarcas.setModel(modelo);
            setSt(getConexao().createStatement());
            String sql ="select id, marca from marcasVeiculos";
            if (idCategoria > 0){
                sql = sql + " WHERE idcategoria = " + idCategoria;
            }
            sql = sql + " order by marca";
            setResultSet(getSt().executeQuery(sql));
            java.sql.ResultSetMetaData rsMd = getResultSet().getMetaData();
            int quantidadeColunas = rsMd.getColumnCount();
            for (int i = 1; i <= quantidadeColunas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            while (getResultSet().next()) {
                Object[] fila = new Object[quantidadeColunas];
                for (int i = 0; i < quantidadeColunas; i++) {
                    fila[i] = getResultSet().getObject(i + 1);
                }
                modelo.addRow(fila);
            }
            getResultSet().close();
            getConexao().close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<MarcasVeiculos> listaMarcasVeiculos() throws SQLException {
        List<MarcasVeiculos> lista = new ArrayList<MarcasVeiculos>();
        try {
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select id , marca  from marcasVeiculos  order by marca"));
            while (getResultSet().next()) {
                MarcasVeiculos cores = new MarcasVeiculos();
                cores.setId(getResultSet().getLong("id"));
                cores.setMarca(getResultSet().getString("marca"));
                lista.add(cores);
            }
        } catch (Exception ex) {
            System.out.println("ERRO->>>" + ex);
        }
        getResultSet().close();
        getConexao().close();
        return lista;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Empresas;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Diego.Avila
 */
public class EmpresasDao extends ConexaoDao {

    public void inserir(Empresas objeto) throws SQLException {
        setSql(" INSERT INTO empresas (nome, cnpj, telefone1, telefone2) "
                + "VALUES (?, ?, ?, ?)");

        try {
            setPs(getConexao().prepareStatement(getSql()));
            getPs().setString(1, objeto.getNome());
            getPs().setString(2, objeto.getCnpj());
            getPs().setString(3, objeto.getTelefone1());
            getPs().setString(4, objeto.getTelefone1());

            if (getPs().executeUpdate() > 0) {

                System.out.println("Inserido ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro no cadastro dao ," + ex.getMessage());

        }
    }

    public void atualizar(Empresas objeto) {
        setSql(" UPDATE empresas set nome= ?, cnpj= ?,  telefone1= ?,  telefone2= ? WHERE id= ?  ");
        try {
            setPs(getConexao().prepareStatement(getSql()));
            getPs().setString(1, objeto.getNome());
            getPs().setString(2, objeto.getCnpj());
            getPs().setString(3, objeto.getTelefone1());
            getPs().setString(4, objeto.getTelefone1());
            getPs().setLong(5, objeto.getId());
            System.out.println(getPs());
            if (getPs().executeUpdate() > 0) {

                System.out.println("Inserido ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro-->>>  " + ex);

        }
    }

    public void deletar(String id) {
        setSql(" delete from empresas where id =" + id);
        try {

            setPs(getConexao().prepareStatement(getSql()));
            if (getPs().executeUpdate() > 0) {
                System.out.println("Deletando ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
    }

    public void listarTabelaEmpresas(JTable tabEmpresas) {

        try {

            DefaultTableModel modelo = new DefaultTableModel();
            tabEmpresas.setModel(modelo);
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select * from empresas order by nome"));
            java.sql.ResultSetMetaData rsMd = getResultSet().getMetaData();
            int quantidadeColunas = rsMd.getColumnCount();
            for (int i = 1; i <= quantidadeColunas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            while (getResultSet().next()) {
                Object[] fila = new Object[quantidadeColunas];
                for (int i = 0; i < quantidadeColunas; i++) {
                    fila[i] = getResultSet().getObject(i + 1);
                }
                modelo.addRow(fila);
            }
            getResultSet().close();
            getConexao().close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

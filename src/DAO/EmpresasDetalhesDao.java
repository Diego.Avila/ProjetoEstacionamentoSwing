/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static DAO.ConexaoDao.getConexao;
import Model.EmpresasDetalhes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Diego.Avila
 */
public class EmpresasDetalhesDao extends ConexaoDao {


     public long inserir(EmpresasDetalhes objeto) throws SQLException {
        setSql(" INSERT INTO empresasDetalhes (placa, idempresas, idveiculos, idenderecos) "
                + "VALUES (?, ?, ?, ?)");

        try {
            setPs(getConexao().prepareStatement(getSql(),getSt().RETURN_GENERATED_KEYS));
            getPs().setString(1, objeto.getPlaca());
            getPs().setLong(2, objeto.getIdempresas());
            getPs().setLong(3, objeto.getIdveiiculos());
            getPs().setLong(4, objeto.getIdenderecos());
            getPs().executeUpdate();
            setResultSet(getPs().getGeneratedKeys());
            if (getResultSet().next()) {
                setRetorno(getResultSet().getLong(1));
                System.out.println("Inserido ---------------id = "+ getRetorno());
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro no cadastro dao ," + ex.getMessage());

        }
        return getRetorno();
    }

    public void atualizar(EmpresasDetalhes objeto) {
        setSql(" UPDATE empresasDetalhes set plca= ?, set idempresas = ?, set idveiculos = ?, set idenderecos = ? WHERE id= ?  ");
        try {
            setPs(getConexao().prepareStatement(getSql()));
            getPs().setString(1, objeto.getPlaca());
            getPs().setLong(2, objeto.getIdempresas());
            getPs().setLong(3, objeto.getIdveiiculos());
            getPs().setLong(4, objeto.getIdenderecos());
            getPs().setLong(5, objeto.getId());
            System.out.println(getPs());
            if (getPs().executeUpdate() > 0) {

                System.out.println("Atualizado ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println("erro-->>>  " + ex);

        }
    }

    
    public void deletar(String id) {
        setSql(" delete from empresasDetalhes where id =" + id);
        try {
            setPs(getConexao().prepareStatement(getSql()));
            if (getPs().executeUpdate() > 0) {
                System.out.println("Deletando ------------------------------------");
            } else {
                System.out.println("falha ------------------------------------");
            }

            getPs().close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
    }

    
    public void listarJTable(JTable tabelaCores) {

        try {

            DefaultTableModel modelo = new DefaultTableModel();
            tabelaCores.setModel(modelo);
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select * from empresasDetahes  order by placa"));
            java.sql.ResultSetMetaData rsMd = getResultSet().getMetaData();
            int quantidadeColunas = rsMd.getColumnCount();
            for (int i = 1; i <= quantidadeColunas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            while (getResultSet().next()) {
                Object[] fila = new Object[quantidadeColunas];
                for (int i = 0; i < quantidadeColunas; i++) {
                    fila[i] = getResultSet().getObject(i + 1);
                }
                modelo.addRow(fila);
            }
            getResultSet().close();
            getConexao().close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<EmpresasDetalhes> listaEmpresasDetalhes() throws SQLException {
        List<EmpresasDetalhes> lista = new ArrayList<EmpresasDetalhes>();
        try {
            setSt(getConexao().createStatement());
            setResultSet(getSt().executeQuery("select id , placa, idempresas, idveiculos, idenderecos from tipoVeiculos  order by nometipo"));
            while (getResultSet().next()) {
                EmpresasDetalhes empresasDetalhes = new EmpresasDetalhes();
                empresasDetalhes.setId(getResultSet().getLong("id"));
                empresasDetalhes.setPlaca(getResultSet().getString("placa"));
                empresasDetalhes.setIdempresas(getResultSet().getLong("idempresas"));
                empresasDetalhes.setIdveiiculos(getResultSet().getLong("idveiculos"));
                empresasDetalhes.setIdenderecos(getResultSet().getLong("idendereco"));
                lista.add(empresasDetalhes);
            }
        } catch (Exception ex) {
            System.out.println("ERRO->>>" + ex);
        }
        getResultSet().close();
        getConexao().close();
        return lista;
    }

}
